CREATE TABLE `address_book` (
  `address_id` int(11) NOT NULL,
  `address_street` varchar(255) NOT NULL,
  `address_country` varchar(255) NOT NULL,
  `address_state` varchar(255) DEFAULT NULL,
  `address_city` varchar(255) DEFAULT NULL,
  `address_zip` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `address_book`
  ADD PRIMARY KEY (`address_id`);

ALTER TABLE `address_book`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;