var ab = {
  list: function () {
  // list() : show all the address entries

    // APPEND FORM DATA
    var data = new FormData();
    data.append('req', 'list');

    // AJAX
    var xhr = new XMLHttpRequest();
    xhr.open('POST', "endpoint/3c-address.php", true);
    xhr.onload = function(){
      if (xhr.status==403 || xhr.status==404) {
        alert("ERROR OPENING FILE!");
      } else {
        document.getElementById("container").innerHTML = this.response;
      }
    };
    xhr.send(data);
  },

  del : function (id) {
  // del() : delete entry
  // PARAM id : address id

    if (confirm("Delete entry?")) {
      // APPEND FORM DATA
      var data = new FormData();
      data.append('req', 'del');
      data.append('id', id);

      // AJAX
      var xhr = new XMLHttpRequest();
      xhr.open('POST', "endpoint/3c-address.php", true);
      xhr.onload = function(){
        if (xhr.status==403 || xhr.status==404) {
          alert("ERROR OPENING FILE!");
        } else {
          if (this.response=="OK") {
            ab.list();
          } else {
            alert("An error has occured!");
          }
        }
      };
      xhr.send(data);
    }
  },

  addEdit : function (id) {
  // addEdit() : show add/edit entry form
  // PARAM id : address id

    // APPEND FORM DATA
    var data = new FormData();
    data.append('req', 'addEdit');
    data.append('id', id);

    // AJAX
    var xhr = new XMLHttpRequest();
    xhr.open('POST', "endpoint/3c-address.php", true);
    xhr.onload = function(){
      document.getElementById("container").innerHTML = this.response;
    };
    xhr.send(data);
  },

  save : function () {
  // save() : save address

    // APPEND FORM DATA
    var data = new FormData();
    var id = document.getElementById("ab_id").value;
    if (id==""){
      data.append('req', 'add');
    } else {
      data.append('req', 'edit');
      data.append('id', id);
    }
    data.append('street', document.getElementById("ab_addr").value);
    data.append('country', document.getElementById("ab_country").value);
    data.append('state', document.getElementById("ab_state").value);
    data.append('city', document.getElementById("ab_city").value);
    data.append('zip', document.getElementById("ab_zip").value);

    // AJAX
    var xhr = new XMLHttpRequest();
    xhr.open('POST', "endpoint/3c-address.php", true);
    xhr.onload = function(){
      if (xhr.status==403 || xhr.status==404) {
        alert("ERROR OPENING FILE!");
      } else {
        if (this.response=="OK") {
          ab.list();
        } else {
          alert("An error has occured!");
        }
      }
    };
    xhr.send(data);
    return false;
  }
};

window.addEventListener("load", ab.list);