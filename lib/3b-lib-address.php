<?php
class Address {
  private $pdo = null;
  private $stmt = null;

  function __construct () {
  // __construct() : connect to the database
  // PARAM : DB_HOST, DB_CHARSET, DB_NAME, DB_USER, DB_PASSWORD

    try {
      $this->pdo = new PDO(
        "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET, DB_USER, DB_PASSWORD, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false
        ]
      );
      return true;
    } catch (Exception $ex) {
      $this->CB->verbose(0, "DB", $ex->getMessage(), "", 1);
    }
  }

  function __destruct () {
  // __destruct() : close connection when done

    if ($this->stmt !== null) {
      $this->stmt = null;
    }
    if ($this->pdo !== null) {
      $this->pdo = null;
    }
  }

  function get ($id){
  // get() : get address book entry
  // PARAM $id : address book ID

   $sql = "SELECT * FROM `address_book` WHERE `address_id`=?";
    $this->stmt = $this->pdo->prepare($sql);
    $this->stmt->execute([$id]);
    $entry = $this->stmt->fetchAll();
    return count($entry)==0 ? false : $entry[0] ;
  }

  function getAll (){
  // get() : get all address book entries

    $sql = "SELECT * FROM `address_book` WHERE 1";
    $this->stmt = $this->pdo->prepare($sql);
    $this->stmt->execute();
    $entry = $this->stmt->fetchAll();
    return count($entry)==0 ? false : $entry ;
  }

  function add ($street, $country, $state="", $city="", $zip) {
  // add() : add a new entry
  // PARAM $street - street address
  //       $country - country
  //       $state - state (optional)
  //       $city - city (optional)
  //       $zip - zip code

    // BASIC QUERY
    $sql = "INSERT INTO `address_book` (`address_street`, `address_country`, `address_zip`";
    $val = " VALUES (?, ?, ?";
    $cond = [$street, $country, $zip];

    // IF STATE PROVIDED
    if ($state != "") {
      $sql .= ", `address_state`";
      $val .= ", ?";
      $cond[] = $state;
    }

    // IF CITY PROVIDED
    if ($city != "") {
      $sql .= ", `address_city`";
      $val .= ", ?";
      $cond[] = $city;
    }

    // FINAL QUERY
    $sql .= ") " . $val . ");";

    try {
      $this->stmt = $this->pdo->prepare($sql);
      $this->stmt->execute($cond);
    } catch (Exception $ex) {
      return false;
    }
    return true;
  }

  function edit ($street, $country, $state="", $city="", $zip, $id) {
  // edit() : update an entry
  // PARAM $street - street address
  //       $country - country
  //       $state - state (optional)
  //       $city - city (optional)
  //       $zip - zip code
  //       $id - address ID

    // BASIC QUERY
    $sql = "UPDATE `address_book` SET `address_street`=?, `address_country`=?, `address_zip`=?";
    $cond = [$street, $country, $zip];

    // STATE
    if ($state == "") {
      $sql .= ", `address_state`=NULL";
    } else {
      $sql .= ", `address_state`=?";
      $cond[] = $state;
    }

    // CITY
    if ($city == "") {
      $sql .= ", `address_city`=NULL";
    } else {
      $sql .= ", `address_city`=?";
      $cond[] = $city;
    }

    // FINAL QUERY
    $sql .= " WHERE `address_id`=?";
    $cond[] = $id;

    try {
      $this->stmt = $this->pdo->prepare($sql);
      $this->stmt->execute($cond);
    } catch (Exception $ex) {
      return false;
    }
    return true;
  }

  function del ($id) {
  // del() : delete an entry
  // PARAM $id - address ID

    $sql = "DELETE FROM `address_book` WHERE `address_id`=?";
    try {
      $this->stmt = $this->pdo->prepare($sql);
      $this->stmt->execute([$id]);
    } catch (Exception $ex) {
      return false;
    }
    return true;
  }
}
?>