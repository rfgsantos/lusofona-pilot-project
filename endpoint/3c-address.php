<?php
// INIT
require dirname(__DIR__) . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "3a-config.php";
require PATH_LIB . "3b-lib-address.php";
$ab = new Address();

// HANDLE REQUEST
switch ($_POST['req']) {
  // INVALID REQUEST
  default:
    echo "Invalid request";
    break;

  // ADD ENTRY
  case "add":
    echo $ab->add($_POST['street'], $_POST['country'], $_POST['state'], $_POST['city'], $_POST['zip']) ? "OK" : "ERR";
    break;

  // EDIT ENTRY
  case "edit":
    echo $ab->edit($_POST['street'], $_POST['country'], $_POST['state'], $_POST['city'], $_POST['zip'], $_POST['id']) ? "OK" : "ERR";
    break;

  // DELETE ENTRY
  case "del":
    echo $ab->del($_POST['id']) ? "OK" : "ERR";
    break;

  // LIST ENTRIES
  case "list":
    $address = $ab->getAll(); ?>
    <h1>MANAGE ADDRESSES</h1>
    <input type="button" value="Add new entry" onclick="ab.addEdit();"/>
    <br><br>
    <?php
    if (is_array($address)) { foreach ($address as $addr) {
      printf("<div>%s %s %s %s %s "
        . "<input type='button' value='Delete' onclick='ab.del(%u)'/>"
        . "<input type='button' value='Edit' onclick='ab.addEdit(%u)'/>"
      . "</div>", 
        $addr['address_street'], $addr['address_country'], $addr['address_state'], $addr['address_city'], $addr['address_zip'],
        $addr['address_id'], $addr['address_id']
      );
    }} else {
      echo "<div>No addresses found</div>";
    }
    break;

  // ADD/EDIT FORM
  case "addEdit":
    $edit = is_numeric($_POST['id']);
    if ($edit) {
      $address = $ab->get($_POST['id']);
    } ?>
    <h1><?=$edit?"EDIT":"ADD"?> ADDRESS</h1>
    <form onsubmit="return ab.save()">
      <input type="hidden" id="ab_id" value="<?=$address['address_id']?>"/>
      Street Address:
      <input type="text" id="ab_addr" required value="<?=$address['address_street']?>"/><br>
      Country:
      <input type="text" id="ab_country" required value="<?=$address['address_country']?>"/><br>
      State:
      <input type="text" id="ab_state" value="<?=$address['address_state']?>"/><br>
      City:
      <input type="text" id="ab_city" value="<?=$address['address_city']?>"/><br>
      Zip Code:
      <input type="text" id="ab_zip" required value="<?=$address['address_zip']?>"/><br><br>
      <input type="button" value="Cancel" onclick="ab.list()"/>
      <input type="submit" value="Save"/>
    </form>
    <?php break;
}
?>